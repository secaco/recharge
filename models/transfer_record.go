/***************************************************
 ** @Desc : This file for B2C转账数据模型，此功能不在开放，已放弃维护
 ** @Time : 2019.04.13 13:30 
 ** @Author : Joker
 ** @File : transfer_record
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.13 13:30
 ** @Software: GoLand
****************************************************/
package models

import (
	"github.com/astaxie/beego/orm"
	"recharge/sys"
	"recharge/utils"
)

type TransferRecord struct {
	Id            int
	MerchantNo    string
	UserId        int
	CreateTime    string
	EditTime      string
	SerialNumber  string
	TrOrderId     string
	TrAmount      float64
	TrAccountType string
	TrProductName string
	Status        string
	TrNoticeUrl   string `orm:"type(text)"`
	Remark        string
	Version       int
	UserName      string `orm:"-"`
}

func (*TransferRecord) TableEngine() string {
	return "INNODB"
}

func (*TransferRecord) TableName() string {
	return TransferRecordTBName()
}

/* *
 * @Description: 添加B2C转账记录
 * @Author: Joker
 * @Date: 2019-4-13 13:51:09
 * @Param: Merchant
 * @return: int: 判断成功的标识
 * @return: int64:	在表中哪一行插入
 **/
func (*TransferRecord) InsertTransferRecord(r TransferRecord) (int, int64) {
	om := orm.NewOrm()
	in, _ := om.QueryTable(TransferRecordTBName()).PrepareInsert()
	id, err := in.Insert(&r)
	if err != nil {
		sys.LogError("InsertTransferRecord failed to insert for: ", err)
		return utils.FAILED_FLAG, id
	}
	return utils.SUCCESS_FLAG, id
}

// 读取单个转账记录通过 订单号
func (*TransferRecord) SelectOneTransferRecordByOrderId(id string) (w TransferRecord) {
	om := orm.NewOrm()
	_, err := om.QueryTable(TransferRecordTBName()).Filter("tr_order_id", id).All(&w)
	if err != nil {
		sys.LogError("SelectOneTransferRecordByOrderId failed to select one:", err)
	}
	return w
}

// 修改转账记录
func (*TransferRecord) UpdateTransferRecord(w TransferRecord) int {
	om := orm.NewOrm()
	_, err := om.QueryTable(TransferRecordTBName()).Filter("tr_order_id", w.TrOrderId).Filter("merchant_no", w.MerchantNo).
		Update(orm.Params{
			"version":   orm.ColValue(orm.ColAdd, 1),
			"edit_time": w.EditTime,
			"status":    w.Status,
			"remark":    w.Remark,
		})
	if err != nil {
		sys.LogError("UpdateTransferRecord failed to update for:", err)
		return utils.FAILED_FLAG
	}
	return utils.SUCCESS_FLAG
}

/*查询分页*/
func (*TransferRecord) SelectTransferRecordListPage(params map[string]interface{}, limit, offset int) (list []TransferRecord) {
	om := orm.NewOrm()
	qt := om.QueryTable(TransferRecordTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	_, err := qt.OrderBy("-create_time").Limit(limit, offset).All(&list)
	if err != nil {
		sys.LogError("SelectTransferRecordListPage failed to query paging for: ", err)
	}
	return list
}

func (*TransferRecord) SelectTransferRecordPageCount(params map[string]interface{}) int {
	om := orm.NewOrm()
	qt := om.QueryTable(TransferRecordTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	cnt, err := qt.Count()
	if err != nil {
		sys.LogError("SelectTransferRecordPageCount failed to count for:", err)
	}
	return int(cnt)
}

//根据条件查询所有转账记录
func (*TransferRecord) SelectAllTransferRecordBy(params map[string]interface{}) (list []TransferRecord) {
	om := orm.NewOrm()
	qt := om.QueryTable(TransferRecordTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	_, err := qt.OrderBy("-create_time").All(&list)
	if err != nil {
		sys.LogError("SelectAllTransferRecordBy failed to query paging for: ", err)
	}
	return list
}

// 读取单个转账记录
func (*TransferRecord) SelectOneTransferRecord(id string) (r TransferRecord) {
	om := orm.NewOrm()
	_, err := om.QueryTable(TransferRecordTBName()).Filter("id", id).All(&r)
	if err != nil {
		sys.LogError("SelectOneTransferRecord failed to select one:", err)
	}
	return r
}
