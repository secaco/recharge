/***************************************************
 ** @Desc : This file for 配置数据库连接
 ** @Time : 2018-12-22 13:55:26
 ** @Author : Joker
 ** @File : init_database.go
 ** @Last Modified by : Joker
 ** @Last Modified time:2018-12-22 13:55:26
 ** @Software: GoLand
****************************************************/
package sys

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

//初始化数据连接
func InitDatabase() bool {
	//读取配置文件，设置数据库参数
	//数据库类别
	dbType := beego.AppConfig.String("db_type")
	//连接名称
	dbAlias := beego.AppConfig.String(dbType + "::db_alias")
	//数据库名称
	dbName := beego.AppConfig.String(dbType + "::db_name")
	//数据库连接用户名
	dbUser := beego.AppConfig.String(dbType + "::db_user")
	//数据库连接用户名
	dbPwd := beego.AppConfig.String(dbType + "::db_pwd")
	//数据库IP（域名）
	dbHost := beego.AppConfig.String(dbType + "::db_host")
	//数据库端口
	dbPort := beego.AppConfig.String(dbType + "::db_port")

	var err error
	switch dbType {
	case "sqlite3":
		err = orm.RegisterDataBase(dbAlias, dbType, dbName)
	case "mysql":
		dbCharset := beego.AppConfig.String(dbType + "::db_charset")
		err = orm.RegisterDriver(dbType, orm.DRMySQL)
		err = orm.RegisterDataBase(dbAlias, dbType, dbUser+":"+dbPwd+"@tcp("+dbHost+":"+
			dbPort+")/"+dbName+"?charset="+dbCharset, 30)
	}

	if err != nil {
		return false
	}
	return true
}
