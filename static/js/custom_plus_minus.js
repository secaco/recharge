/***************************************************
 ** @Desc : This file for 自定义加减款js
 ** @Time : 2019.05.22 15:19
 ** @Author : Joker
 ** @File : custom_plus_minus
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.05.22 15:19
 ** @Software: GoLand
 ****************************************************/

let plus_minus = {
    get_after_three_days: function () {
        let now = new Date();

        let year = now.getFullYear();       //年
        let month = now.getMonth() + 1;     //月
        let day = now.getDate();            //日

        let clock = year + "-";

        if (month < 10) clock += "0";
        if (day < 3) month -= 1;
        clock += month + "-";

        if (day < 3) {
            day = 28;
        } else if (day < 10) {
            clock += "0";
            day -= 2;
        } else {
            day -= 2;
        }
        clock += day + " 00:02:00";

        return (clock);
    },
    do_paging: function () {
        let datetimepicker1 = $("#datetimepicker1").val();
        let datetimepicker2 = $("#datetimepicker2").val();
        let _userName = $("#uName").val();
        $.ajax({ //去后台查询第一页数据
            type: "GET",
            url: "/merchant/list_plus_minus/",
            data: {
                page: '1',
                limit: "20",
                start: datetimepicker1,
                end: datetimepicker2,
                _userName: _userName,
            }, //参数：当前页为1
            success: function (data) {
                plus_minus.show_data(data.root);//处理第一页的数据

                let options = {//根据后台返回的分页相关信息，设置插件参数
                    bootstrapMajorVersion: 3, //
                    currentPage: data.page, //当前页数
                    totalPages: data.totalPage, //总页数
                    numberOfPages: data.limit,//每页记录数
                    itemTexts: function (type, page) {//设置分页按钮显示字体样式
                        switch (type) {
                            case "first":
                                return "首页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "末页";
                            case "page":
                                return page;
                        }
                    },
                    onPageClicked: function (event, originalEvent, type, page) {//分页按钮点击事件
                        $.ajax({//根据page去后台加载数据
                            url: "/merchant/list_plus_minus/",
                            type: "GET",
                            data: {
                                page: page,
                                start: datetimepicker1,
                                end: datetimepicker2,
                                _userName: _userName,
                            },
                            success: function (data) {
                                plus_minus.show_data(data.root);//处理数据
                            }
                        });
                    }
                };

                $('#xf_page').bootstrapPaginator(options);//设置分页
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    show_data: function (list) {
        let con = "";
        $.each(list, function (index, item) {
            con += "<tr><td><a>" + (index + 1) + "</a></td>";
            con += "<td>" + item.UserName + "</td>";
            con += "<td>" + item.Addition.toFixed(2) + "</td>";
            con += "<td>" + item.Deduction.toFixed(2) + "</td>";
            con += "<td>" + item.HandingFee.toFixed(2) + "</td>";
            con += "<td>" + item.EditTime + "</td>";
            con += "</tr>";
        });
        $("#your_showtime").html(con);
    },
};